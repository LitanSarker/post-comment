import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, style, animate, transition } from '@angular/animations';


import { MessageService } from 'primeng/api';
import { first } from 'rxjs/operators';
import { UserIdleService } from 'angular-user-idle';
import { Subject } from 'rxjs';
@Component({
    selector: 'app', animations: [
        trigger(
            'menu-annimation', [
                transition(':enter', [
                    style({ transform: 'translateX(-120%)', opacity: 0.5 }),
                    animate('400ms ease-out', style({ transform: 'translateX(0)', opacity: 1 }))
                ]),
                transition(':leave', [
                    style({ transform: 'translateX(0)', opacity: 1 }),
                    animate('100ms ease-out', style({ transform: 'translateX(-60%)', opacity: 0.6 }))
                ])
            ]
        )
    ],
    templateUrl: 'app.component.html',
    encapsulation: ViewEncapsulation.None,
    providers: [MessageService]
})

export class AppComponent implements OnInit {

    
    constructor(
        private router: Router,     
        private messageService: MessageService
    ) {

        
    }

    ngOnInit() {
       
      
    }
   

   

    

}