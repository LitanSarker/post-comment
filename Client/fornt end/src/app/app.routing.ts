import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home';


const appRoutes: Routes = [
    { path: 'home', component: HomeComponent},
    
    {
        path: "",
        pathMatch: "full",
        redirectTo: "home"
    },
    { path: '**', redirectTo: 'home' }
];

export const routing = RouterModule.forRoot(appRoutes, {
    useHash: true,
    enableTracing: false,
    scrollPositionRestoration: 'enabled'
});