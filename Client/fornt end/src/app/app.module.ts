import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { HomeComponent } from './components/home/home.component';

import { FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/api';
import {  ConfirmationService } from 'primeng/api';

import { SlideMenuModule } from 'primeng/slidemenu';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ChartModule } from 'primeng/chart';
import { TreeModule } from 'primeng/tree';
import { MenubarModule } from 'primeng/menubar';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { SplitButtonModule } from 'primeng/splitbutton';
import { TableModule } from 'primeng/table';
import { MegaMenuModule } from 'primeng/megamenu';
import { StepsModule } from 'primeng/steps';
import { MenuModule } from 'primeng/menu';
import { PanelModule } from 'primeng/panel';
import { FieldsetModule } from 'primeng/fieldset';
import { CalendarModule } from 'primeng/calendar';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DropdownModule } from 'primeng/dropdown';
import { InputMaskModule } from 'primeng/inputmask';
import { KeyFilterModule } from 'primeng/keyfilter';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { SliderModule } from 'primeng/slider';
import { MultiSelectModule } from 'primeng/multiselect';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { PasswordModule } from 'primeng/password';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputSwitchModule } from 'primeng/inputswitch';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { TabViewModule } from 'primeng/tabview';
import { TabMenuModule } from 'primeng/tabmenu';
import { AccordionModule } from 'primeng/accordion';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { TreeTableModule } from 'primeng/treetable';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { CarouselModule } from 'primeng/carousel';
import { SidebarModule } from 'primeng/sidebar';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { CheckboxModule } from 'primeng/checkbox';

import { UserIdleModule } from 'angular-user-idle';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';



@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        FormsModule,
        SlideMenuModule,
        PanelMenuModule,
        ScrollPanelModule,
        ChartModule,
        TreeModule,
        MenubarModule,
        CardModule,
        InputTextModule,
        SplitButtonModule,
        TableModule,
        MegaMenuModule,
        MenuModule,
        StepsModule,
        PanelModule,
        FieldsetModule,
        CalendarModule,
        SelectButtonModule,
        DropdownModule,
        InputMaskModule,
        KeyFilterModule,
        OverlayPanelModule,
        SliderModule,
        MultiSelectModule,
        ConfirmDialogModule,
        ToastModule,
        DialogModule,
        PasswordModule,
        InputTextareaModule,
        InputSwitchModule,
        MessageModule,
        MessagesModule,
        NgbModule,
        NgxQRCodeModule,
        TabViewModule,
        TabMenuModule,
        AccordionModule,
        ProgressSpinnerModule,
        OrganizationChartModule,
        TreeTableModule,
        TieredMenuModule,
        CarouselModule,
        SidebarModule,
        PdfJsViewerModule,
        TriStateCheckboxModule,
        ToggleButtonModule,
        CheckboxModule,
        UserIdleModule.forRoot({ idle: 900, timeout: 300, ping: 120 })
    ],
    declarations: [
        AppComponent,
       
        HomeComponent
       
    ],
    providers: [
       
        MessageService, ConfirmationService
        //TitleService              
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }