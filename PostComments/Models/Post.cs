﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostComments.Models
{
    public class Post
    {
        public Post()
        {
            PostComments = new HashSet<PostComments>();
        }
        public int Id { get; set; }
        public string PostName { get; set; }
        public string Author { get; set; }
        public DateTime? PostedDate { get; set; }

        public ICollection<PostComments> PostComments { get; set; }
    }
}
