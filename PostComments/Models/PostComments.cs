﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostComments.Models
{
    public class PostComments
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string CommentDescription { get; set; }
        public string CommentAuthor { get; set; }
        public DateTime? CommentedDate { get; set; }
        public bool IsLike { get; set; }
        


        public Post Post { get; set; }
    }
}
