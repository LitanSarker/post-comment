﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostComments.Models
{
    public class PostCommentContext : DbContext
    {
        public PostCommentContext()
        {
        }

        public PostCommentContext(DbContextOptions<PostCommentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<PostComments> PostComments { get; set; }

       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.PostName)
                    .HasColumnName("POST_NAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Author)
                    .HasColumnName("AUTHOR")
                    .HasMaxLength(255)
                    .IsUnicode(false);
                entity.Property(e => e.PostedDate)
                   .HasColumnName("POSTED_DATE")
                     .HasColumnType("datetime");
            });

            modelBuilder.Entity<PostComments>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.PostId).HasColumnName("POST_ID");

                entity.Property(e => e.CommentDescription)
                    .HasColumnName("COMMENT_DESCRIPTION")
                    .IsUnicode(false);
                entity.Property(e => e.CommentAuthor)
                    .HasColumnName("COMMENT_AUTHOR")
                    .IsUnicode(false);

                entity.Property(e => e.CommentedDate)
                    .HasColumnName("COMMENTED_DATE")
                      .HasColumnType("datetime");


                entity.Property(e => e.IsLike)
                    .HasColumnName("IS_LIKE");
                     

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.PostComments)
                    .HasForeignKey(d => d.PostId)
                    .HasConstraintName("FK__PostComments__Post_I__1273C1CD");
            });
        }
    }
}
