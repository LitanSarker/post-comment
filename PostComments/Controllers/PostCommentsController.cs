﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostComments.Repository;

namespace PostComments.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostCommentsController : ControllerBase
    {
        IPostRepository postRepository;
        public PostCommentsController(IPostRepository _postRepository)
        {
            postRepository = _postRepository;
        }

        [HttpGet]
        [Route("GetPostComments")]
        public async Task<IActionResult> GetPostComments()
        {
            try
            {
                var postComments = await postRepository.GetVMPostComments();
                if (postComments == null)
                {
                    return NotFound();
                }

                return Ok(postComments);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}