﻿using Microsoft.EntityFrameworkCore;
using PostComments.Models;
using PostComments.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostComments.Repository
{
    public interface IPostRepository
    {
        Task<List<Post>> GetPosts();

        Task<List<VMPostComments>> GetVMPostComments();

        //Task<PostViewModel> GetPost(int? postId);

        //Task<int> AddPost(Post post);

        //Task<int> DeletePost(int? postId);

        //Task UpdatePost(Post post);
    }
    public class PostRepository: IPostRepository
    {
        PostCommentContext db;
        public PostRepository(PostCommentContext _db)
        {
            db = _db;
        }

        public async Task<List<Post>> GetPosts()
        {
            if (db != null)
            {
                return await db.Post.ToListAsync();
            }

            return null;
        }

        public async Task<List<VMPostComments>> GetVMPostComments()
        {
            if (db != null)
            {

                return await (from p in db.Post
                              from pc in p.PostComments
                              where pc.PostId == p.Id
                              select new VMPostComments
                              {
                                  PostName = p.PostName,
                                  Author = p.Author,
                                  PostedDate = p.PostedDate,
                                  TotalComments = 100
                                  //VMCommentList = p.PostComments


                              }).ToListAsync();




            }

            return null;
        }

        //public async Task<PostViewModel> GetPost(int? postId)
        //{
        //    if (db != null)
        //    {
        //        return await (from p in db.Post
        //                      from c in db.Category
        //                      where p.PostId == postId
        //                      select new PostViewModel
        //                      {
        //                          PostId = p.PostId,
        //                          Title = p.Title,
        //                          Description = p.Description,
        //                          CategoryId = p.CategoryId,
        //                          CategoryName = c.Name,
        //                          CreatedDate = p.CreatedDate
        //                      }).FirstOrDefaultAsync();
        //    }

        //    return null;
        //}

        //public async Task<int> AddPost(Post post)
        //{
        //    if (db != null)
        //    {
        //        await db.Post.AddAsync(post);
        //        await db.SaveChangesAsync();

        //        return post.PostId;
        //    }

        //    return 0;
        //}

        //public async Task<int> DeletePost(int? postId)
        //{
        //    int result = 0;

        //    if (db != null)
        //    {
        //        //Find the post for specific post id
        //        var post = await db.Post.FirstOrDefaultAsync(x => x.PostId == postId);

        //        if (post != null)
        //        {
        //            //Delete that post
        //            db.Post.Remove(post);

        //            //Commit the transaction
        //            result = await db.SaveChangesAsync();
        //        }
        //        return result;
        //    }

        //    return result;
        //}


        //public async Task UpdatePost(Post post)
        //{
        //    if (db != null)
        //    {
        //        //Delete that post
        //        db.Post.Update(post);

        //        //Commit the transaction
        //        await db.SaveChangesAsync();
        //    }
        //}


    }
}
