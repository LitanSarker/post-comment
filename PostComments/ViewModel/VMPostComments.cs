﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostComments.ViewModel
{
    public class VMPostComments
    {
        public string PostName { get; set; }
        public string Author { get; set; }
        public DateTime? PostedDate { get; set; }
        public int TotalComments { get; set; }

        public List<VMComment> VMCommentList { get; set; }


    }

    public class VMComment
    {
        public string CommentDescription { get; set; }
        public string CommentAuthor { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }
    }
}
